package com.xaphe.action.user.security;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import vn.xaphe.shop.online.domain.mapper.UsersMapper;
import vn.xaphe.shop.online.domain.model.Users;
import vn.xaphe.shop.online.domain.model.UsersExample;

import java.util.ArrayList;
import java.util.List;


public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    UsersMapper usersMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        UsersExample ex = new UsersExample();
        ex.getOredCriteria().add(ex.createCriteria().andEmailEqualTo(s));
        ex.setLimit(1);

        List<Users> usersList = usersMapper.selectByExample(ex);
        Users users = usersList.get(0);

        List<GrantedAuthority> list = new ArrayList<>();
        list.add(new GrantedAuthorityImpl("ROLE_USER"));
        UserDetails userDetails = new UserDetailsImpl(users.getEmail(), users.getPassword(), list);

        return userDetails;

    }
}

package com.xaphe.action.user.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import vn.xaphe.shop.online.domain.mapper.RolesMapper;
import vn.xaphe.shop.online.domain.mapper.UsersMapper;
import vn.xaphe.shop.online.domain.model.Roles;
import vn.xaphe.shop.online.domain.model.Users;
import vn.xaphe.shop.online.domain.model.UsersExample;

import java.util.ArrayList;
import java.util.List;

public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    UsersMapper usersMapper;
    @Autowired
    RolesMapper rolesMapper;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        UsersExample ex = new UsersExample();
        ex.getOredCriteria().add(ex.createCriteria().andEmailEqualTo(name));
        ex.setLimit(1);

        List<Users> usersList = usersMapper.selectByExample(ex);
        if (usersList.size() > 0) {
            Users users = usersList.get(0);
            if (password.equals(users.getPassword())) {
                Roles roles = rolesMapper.selectByPrimaryKey(users.getRoleId());
                List<GrantedAuthority> list = new ArrayList<>();
                list.add(new GrantedAuthorityImpl(roles.getName()));

                return new UsernamePasswordAuthenticationToken(name, password, list);
            } else {
                throw new BadCredentialsException("Invalid password");
            }
        } else {
            throw new BadCredentialsException("Email not found!");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}

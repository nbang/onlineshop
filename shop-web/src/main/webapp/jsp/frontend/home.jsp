<%@include file="/jsp/includes/taglib.inc"%>

<html>
<head>
<title><s:text name="label.category.title" /></title>
<link href="<s:url value='/styles/site/page-category.css'/>"
	rel="stylesheet" type="text/css" media="all" />

</head>
<body>
<div class="container">
	<ul class="thumbnails">
		<li class="span4">
			<div class="thumbnail">
				<img src="http://placehold.it/320x200" alt="ALT NAME">
				<div class="caption">
					<h3>Header Name</h3>
					<p>Description</p>
					<p align="center">
						<a href="#" class="btn btn-primary btn-block">Open</a>
					</p>
				</div>
			</div>
		</li>
		<li class="span4">
			<div class="thumbnail">
				<img src="http://placehold.it/320x200" alt="ALT NAME">
				<div class="caption">
					<h3>Header Name</h3>
					<p>Description</p>
					<p align="center">
						<a href="#" class="btn btn-primary btn-block">Open</a>
					</p>
				</div>
			</div>
		</li>
		<li class="span4">
			<div class="thumbnail">
				<img src="http://placehold.it/320x200" alt="ALT NAME">
				<div class="caption">
					<h3>Header Name</h3>
					<p>Description</p>
					<p align="center">
						<a href="#" class="btn btn-primary btn-block">Open</a>
					</p>
				</div>
			</div>
		</li>
		<li class="span4">
			<div class="thumbnail">
				<img src="http://placehold.it/320x200" alt="ALT NAME">
				<div class="caption">
					<h3>Header Name</h3>
					<p>Description</p>
					<p align="center">
						<a href="#" class="btn btn-primary btn-block">Open</a>
					</p>
				</div>
			</div>
		</li>
		<li class="span4">
			<div class="thumbnail">
				<img src="http://placehold.it/320x200" alt="ALT NAME">
				<div class="caption">
					<h3>Header Name</h3>
					<p>Description</p>
					<p align="center">
						<a href="#" class="btn btn-primary btn-block">Open</a>
					</p>
				</div>
			</div>
		</li>
		<li class="span4">
			<div class="thumbnail">
				<img src="http://placehold.it/320x200" alt="ALT NAME">
				<div class="caption">
					<h3>Header Name</h3>
					<p>Description</p>
					<p align="center">
						<a href="#" class="btn btn-primary btn-block">Open</a>
					</p>
				</div>
			</div>
		</li>
	</ul>
</div>
</body>
</html>

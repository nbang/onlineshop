<%@include file="/jsp/includes/taglib.inc"%>

<html>
<head>
<title><s:text name="label.item.detail.title" /></title>
</head>
<body>
	<div class="container">
		<ul>
			<li>
				<div class="thumbnail">
					<img src="http://placehold.it/320x200" alt="ALT NAME">
					<div class="caption">
						<h4><s:text name="label.item.name" />:&nbsp;<s:property value="itemName"/></h4>
						<p><s:text name="label.item.price" />:&nbsp;<s:property value="price"/></p>
						<p><s:text name="label.item.description" />:&nbsp;<s:property value="description"/></p>
						<p align="center">
							<a href="#" class="btn btn-primary btn-block">Open</a>
						</p>
					</div>
				</div>
			</li>
		</ul>
	</div>
</body>
</html>

<%@include file="/jsp/includes/taglib.inc"%>

<html>
<head>
<title><s:text name="label.item.title" /></title>
<link href="<s:url value='/styles/site/datatable_style.css'/>"
	rel="stylesheet" type="text/css" media="all" />
</head>
<body id="page_category">
	<div class="container">
		<s:if test="(itemInfoList == null) || (itemInfoList.isEmpty)">
			<s:text name="msg.err.001" />
		</s:if>
		<s:else>
			<div class="dataContent">
				<h3 class="modal-header">
					<s:text name="label.item.list.heading" />
				</h3>
				<table class="table">
					<tr>
						<th style="min-width: 15px;">#</th>
						<th style="min-width: 120px;"><s:text name="label.item.name" /></th>
						<th style="min-width: 120px;"><s:text
								name="label.item.category" /></th>
						<th style="min-width: 120px;"><s:text name="label.item.price" /></th>
						<th style="min-width: 120px;"><s:text
								name="label.item.description" /></th>
					</tr>
					<s:iterator value="itemInfoList" var="item" status="status">
						<tr
							class="<s:if test="#status.odd == true ">odd</s:if><s:else>even</s:else>">
							<td align="right"><s:property value="%{#status.index + 1}" /></td>
							<td><s:a action="detail" namespace="/item">
									<s:param name="id" value="%{id}" />
									<s:property value="%{name}"></s:property>
							</s:a></td>
							<td><s:property value="%{categoryName}"></s:property></td>
							<td><s:property value="%{price}"></s:property></td>
							<td><s:property value="%{description}"></s:property></td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</s:else>
	</div>
</body>
</html>
<%@include file="/jsp/includes/taglib.inc"%>

<html>
<head>
<title><s:text name="label.user.title" /></title>
<link href="<s:url value='/styles/site/page-user.css'/>"
	rel="stylesheet" type="text/css" media="all" />
</head>
<body id="page_user">

	<div class="container">
		<s:form action="doEdit" cssClass="form_save">
			<s:hidden name="id" />
			<h3 class="modal-header">
				<s:text name="label.user.edit.heading" />
			</h3>
			<div class="form_save-body">
				<s:textfield name="userName"
					placeholder="%{getText('label.user.name')}"
					cssClass="form-control" />
				<s:fielderror fieldName="userNameError"></s:fielderror>
				<s:textfield name="email"
					placeholder="%{getText('label.user.email')}"
					cssClass="form-control" />
				<s:fielderror fieldName="emailError"></s:fielderror>
			</div>
			<div class="form_save-control modal-footer">
				<button type="button" class="btn btn-default" onclick="history.back();">
					<i class="icon icon-level-up"></i>&nbsp;
					<s:text name="label.cancel" />
				</button>
				<button type="submit" class="btn btn-default">
					<i class="icon icon-edit"></i>&nbsp;
					<s:text name="label.save" />
				</button>
			</div>
		</s:form>
	</div>
</body>
</html>

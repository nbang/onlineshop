<%@include file="/jsp/includes/taglib.inc"%>

<html>
<head>
<title><s:text name="label.item.title" /></title>
<link href="<s:url value='/styles/site/page-category.css'/>"
	rel="stylesheet" type="text/css" media="all" />
<link href="<s:url value='/styles/site/datatable_style.css'/>"
	rel="stylesheet" type="text/css" media="all" />
</head>
<body id="page_category">
<div class="container">
	<div class="dataContent">
		<h3 class="modal-header">
			<s:text name="label.item.list.heading" />
		</h3>
		<s:if test="(itemDetailDTOList == null) || (itemDetailDTOList.isEmpty)">
			<s:text name="msg.err.001" />
		</s:if>
		<h4>
			<s:url id="createURL" action="create"></s:url>
			<s:a href="%{createURL}" cssClass="btn btn-primary">
				<s:text name="label.item.add.new" />
			</s:a>
		</h4>
		<div class="menu">
			<div class="accordion">
				<div class="accordion-group">
					<s:iterator value="itemDetailDTOList" var="itemDetailDTO" status="status">
						<div class="accordion-heading category">
							<img src="http://placehold.it/100x30" alt="category flag"
								style="float: left; margin: 3px 10px 0 3px; text-align: center;" />
							<a class="accordion-toggle" data-toggle="collapse"
								href='#category<s:property value="%{#status.index + 1}"/>'><s:property value="categoryName" /></a>
						</div>
						<div id='category<s:property value="%{#status.index + 1}"/>' class="accordion-body collapse">
							<div class="accordion-inner">
								<table class="table table-striped table-condensed">
									<thead>
										<tr>
											<th><s:text name="label.edit" /></th>
											<th><s:text name="label.delete" /></th>
											<th><s:text name="label.item.name" /></th>
											<th><s:text name="label.item.price" /></th>
											<th><s:text name="label.item.description" /></th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="itemList" var="item">
											<tr>
												<td><s:url id="editURL" action="edit">
														<s:param name="id" value="%{id}" />
													</s:url> <s:a href="%{editURL}">
														<s:text name="label.edit" />
													</s:a></td>
												<td><s:url id="deleteURL" action="doDelete">
														<s:param name="id" value="%{id}" />
													</s:url> <s:a href="%{deleteURL}">
														<s:text name="label.delete" />
													</s:a></td>
												<td><s:property value="name" /></td>
												<td><s:property value="price" /></td>
												<td><s:property value="description" /></td>
												<td><s:property value="flgDel" /></td>
											</tr>
										</s:iterator>
									</tbody>
								</table>
							</div>
						</div>
					</s:iterator>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
<%@include file="/jsp/includes/taglib.inc"%>

<html>
<head>
<title><s:text name="label.item.title" /></title>
<link href="<s:url value='/styles/site/page-category.css'/>"
	rel="stylesheet" type="text/css" media="all" />
</head>
<body id="page_category">

	<div class="container">
		<s:form action="doEdit" cssClass="form_save">
			<s:hidden name="id" />
			<h3 class="modal-header">
				<s:text name="label.item.edit.heading" />
			</h3>
			<div class="form_save-body">
				<s:select name="categoryId" list="categoryList"
					placeholder="%{getText('label.category.name')}" headerKey=""
					headerValue="Select a category" listKey="id" listValue="name"
					value="categoryId" cssClass="form-control chosen-select" />
				<s:fielderror fieldName="categoryName"></s:fielderror>
				<s:textfield name="itemName"
					placeholder="%{getText('label.item.name')}" cssClass="form-control" />
				<s:fielderror fieldName="itemName"></s:fielderror>
				<s:textfield name="price"
					placeholder="%{getText('label.item.price')}" cssClass="form-control" />
				<s:fielderror fieldName="price"></s:fielderror>
				<s:textarea name="description" placeholder="%{getText('label.item.description')}" cssStyle="width: 98%; height: 200px;" required="true"/>
			</div>
			<div class="form_save-control modal-footer">
				<button type="button" class="btn btn-default" onclick="history.back();">
					<i class="icon icon-level-up"></i>&nbsp;
					<s:text name="label.cancel" />
				</button>
				<button type="submit" class="btn btn-default">
					<i class="icon icon-edit"></i>&nbsp;
					<s:text name="label.save" />
				</button>
			</div>
		</s:form>
	</div>
</body>
</html>

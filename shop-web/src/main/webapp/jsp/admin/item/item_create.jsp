<%@include file="/jsp/includes/taglib.inc"%>

<html>
<head>
<title><s:text name="label.category.title" /></title>
<link href="<s:url value='/styles/site/page-category.css'/>"
	rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<s:url value='/scripts/nicEdit/nicEdit.js'/>"></script>
<script type="text/javascript">
//<![CDATA[
bkLib.onDomLoaded(function() {
	new nicEditor(
			{
				maxHeight: 400
			}).panelInstance('description');
});
//]]>
</script>
</head>
<body id="page_category">

	<div class="container">
		<s:form action="doCreate" cssClass="form_save">
			<s:hidden name="id" />
			<h3 class="modal-header">
				<s:text name="label.item.add.heading" />
			</h3>
			<div class="form_save-body">
				<s:select name="categoryId" list="categoryList"
					placeholder="%{getText('label.category.name')}" headerKey=""
					headerValue="Select a category" listKey="id" listValue="name"
					value="id" cssClass="form-control chosen-select" />
				<s:fielderror fieldName="categoryName"></s:fielderror>
				<s:textfield name="itemName" key="label.item.name"
					placeholder="%{getText('label.item.name')}" cssClass="form-control" />
				<s:fielderror fieldName="itemName" />
				<s:textfield name="price"
					placeholder="%{getText('label.item.price')}"
					cssClass="form-control" />
				<s:fielderror fieldName="price" />
				<s:textarea name="description" key="label.item.description" cssStyle="width: 98%; height: 200px;" required="true"/>
			</div>
			<div class="form_save-control modal-footer">
				<button type="submit" class="btn btn-default">
					<i class="icon icon-ok"></i>&nbsp;
					<s:text name="label.save" />
				</button>
			</div>
		</s:form>
	</div>
</body>
</html>

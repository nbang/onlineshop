<%@include file="/jsp/includes/taglib.inc"%>

<html>
<head>
<title><s:text name="label.category.title" /></title>
<link href="<s:url value='/styles/site/page-category.css'/>"
	rel="stylesheet" type="text/css" media="all" />
<link href="<s:url value='/styles/site/datatable_style.css'/>"
	rel="stylesheet" type="text/css" media="all" />
</head>
<body id="page_category">
	<div class="container">
		<s:if test="(categoryList == null) || (categoryList.isEmpty)">
			<s:text name="msg.err.001" />
			<h4>
				<s:url id="createURL" action="create"></s:url>
				<s:a href="%{createURL}" cssClass="btn btn-primary">
					<s:text name="label.category.add.new" />
				</s:a>
			</h4>
		</s:if>
		<s:else>
			<div class="dataContent">
				<h3 class="modal-header">
					<s:text name="label.category.list.heading" />
				</h3>
				<h4>
					<s:url id="createURL" action="create"></s:url>
					<s:a href="%{createURL}" cssClass="btn btn-primary">
						<s:text name="label.category.add.new" />
					</s:a>
				</h4>
				<table class="table">
					<tr>
						<th style="min-width: 15px; width: 50px;">#</th>
						<th style="min-width: 60px; width: 50px;"><s:text
								name="label.edit" /></th>
						<th style="min-width: 60px; width: 50px;"><s:text
								name="label.delete" /></th>
						<th style="min-width: 160px;"><s:text
								name="label.category.name" /></th>
					</tr>
					<s:iterator value="categoryList" var="category" status="status">
						<tr
							class="<s:if test="#status.odd == true ">odd</s:if><s:else>even</s:else>">
							<td align="right"><s:property value="%{#status.index + 1}" /></td>
							<td><s:url id="editURL" action="edit">
									<s:param name="id" value="%{id}"></s:param>
								</s:url> <s:a href="%{editURL}">
									<s:text name="label.edit" />
								</s:a></td>
							<td><s:url id="deleteURL" action="doDelete">
									<s:param name="id" value="%{id}"></s:param>
								</s:url> <s:a href="%{deleteURL}">
									<s:text name="label.delete" />
								</s:a></td>
							<td><s:property value="name" /></td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</s:else>
	</div>
</body>
</html>
<%@include file="/jsp/includes/taglib.inc"%>

<html>
<head>
<title><s:text name="label.admin.menu.title" /></title>
</head>
<body id="page-menu">
	
	<div class="menu-container">

		<s:url id="listCategoryURL" namespace="/" action="home" />
		<s:a href="%{listCategoryURL}" cssClass="menu-item item-w2">
			<div class="icon"><i class="icon-home"></i></div>
			<div class="title">Home</div>
		</s:a>

		<s:url id="listCategoryURL" namespace="/admin/category" action="list" />
		<s:a href="%{listCategoryURL}" cssClass="menu-item color-2">
			<div class="icon"><i class="icon-list"></i></div>
			<div class="title">Category</div>
		</s:a>

		<s:url id="listItemURL" namespace="/admin/item" action="list" />
		<s:a href="%{listItemURL}" cssClass="menu-item color-5">
			<div class="icon"><i class="icon-camera"></i></div>
			<div class="title">Product</div>
		</s:a>

		<s:url id="listUserURL" namespace="/admin/user" action="list" />
		<s:a href="%{listUserURL}" cssClass="menu-item color-9">
			<div class="icon"><i class="icon-user"></i></div>
			<div class="title">User</div>
		</s:a>
		
		<%-- <s:a action="dashboard" namespace="/" cssClass="menu-item item-w2 color-2">
			<div class="icon"><i class="icon-calendar"></i></div>
			<div class="title">Timecard</div>
		</s:a>
		
		<s:a action="dashboard" namespace="/" cssClass="menu-item  color-10">
			<div class="icon"><i class="icon-camera-retro"></i></div>
			<div class="title">Device</div>
		</s:a> --%>
		
		<div class="clearfix"></div>
		
	</div>
</body>
</html>

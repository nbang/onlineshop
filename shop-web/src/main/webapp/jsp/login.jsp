<%@include file="/jsp/includes/taglib.inc" %>
<html>
<head>
<title>Struts Tutorial</title>
</head>

<body>
<h2>Login Page</h2>
<s:actionerror/>

    <form id="LoginForm" method="POST" action="<s:url value='j_spring_security_check'/>" >

    <s:textfield name="j_username" key="label.username" size="20" />
	<s:password name="j_password" key="label.password" size="20" />
	<s:submit method="execute" key="label.login" align="center" />

        </form>
</body>
</html>
  
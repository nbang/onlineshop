﻿/**
* JCMS Library
* @created Nguyen Phu Cuong
* @date  2012-10-23
*/
if (typeof JCMS == "undefined" || !JCMS) {
    var JCMS = {};
}

JCMS.namespace = function () {
    var a = arguments, o = null, i, j, d;
    for (i = 0; i < a.length; i = i + 1) {
        d = ("" + a[i]).split(".");
        o = JCMS;

        for (j = (d[0] == "JCMS") ? 1 : 0; j < d.length; j = j + 1) {
            o[d[j]] = o[d[j]] || {};
            o = o[d[j]];
        }
    }

    return o;
}

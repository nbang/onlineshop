﻿/**
* Page 0309 - Group Week Schedule
* @created Nguyen Phu Cuong
* @date  2013-08-01
*/

JCMS.namespace("JCMS.pages");

/**
 * Page 0309 class
 * Group Week Schedule
 * @class
 * @author Phu-Cuong
 */
JCMS.pages.Page0309 = function(option) {
	
	/**
	 * Option
	 * @private
	 */
	this._option = option;
	
}

//===== Prototype =====
/**
 * Init
 */
JCMS.pages.Page0309.prototype.init = function() {
	var self = this;
}

//===== Static =====
/**
* Startup function. 
* Call this from view
* @param option
*/
JCMS.pages.Page0309.startup = function (option) {

    var page = new JCMS.pages.Page0309(option);
    page.init();

}
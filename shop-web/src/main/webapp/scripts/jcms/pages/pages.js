﻿/**
* Page List User
* @created Nguyen Phu Cuong
* @date  2012-10-23
*/

JCMS.namespace("JCMS.pages");

//===== Static ======
/**
 * Startup application
 */
JCMS.pages.startUp = function (option) {
    // Initialize application
    var app = new JCMS.pages.App();

    // Load message for localization
    //app.loadMessages(option.lang);

    //app.loadHistory();  
    app.init();
}

/**
 * Application class
 * @class
 */
JCMS.pages.App = function () {
}

/**
 * Init application
 */
JCMS.pages.App.prototype.init = function(){
	$('.datepicker').pickadate({format: "yyyy-mm-dd"});	
	$('.timepicker').pickatime({interval: 5, format: "HH:i"})
	
	$('.chosen-select').chosen({});
}

/**
* Load message for localization
* @param none
*/
JCMS.pages.App.prototype.loadMessages = function (lang) {
    jQuery.i18n.properties({
        path: 'scripts/JCMS/messages/',
        mode: 'map',
        language: lang,
        callback: function () {            
        }
    });
}

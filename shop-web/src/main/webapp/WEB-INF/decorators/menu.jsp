<!DOCTYPE html>

<%@include file="/jsp/includes/taglib.inc" %>

<%-- <%@ page import="vn.ecreation.jcms.business.common.UserInfo" %> --%>

<html>
<head>
	<title><decorator:title default="Blank Page"/></title>
	
	<s:head/>

    <link href="<s:url value='/styles/site/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/styles/site/font-awesome/css/font-awesome.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    
    <!--[if IE 7]>
		<link rel="stylesheet" href="<s:url value='/styles/site/font-awesome/font-awesome-ie7.min.css'/>">
	<![endif]-->
	
	<link href="<s:url value='/styles/site/layout-menu.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/styles/site/styles.css'/>" rel="stylesheet" type="text/css" media="all"/>
    
    <!--[if lt IE 9]>
		<script src="<s:url value='/scripts/html5/html5shiv.js'/>"></script>
	<![endif]-->
	
	<script src="<s:url value='/scripts/jquery/jquery-1.10.2.min.js'/>"></script>
	<script src="<s:url value='/scripts/bootstrap/bootstrap.min.js'/>"></script>
	
	<decorator:head/>
</head>
<body<decorator:getProperty property="body.id" writeEntireProperty="true" />>

<%-- 	<% 
	UserInfo userInfo = UserInfo.getFromSession();
	%> --%>

	<div id="pageHeader">
		<div class="container">
			<div class="wrap-content">
				<div class="logo-app"></div>

				<ul class="nav nav-pills pull-right" id="navUser">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class='icon-user'></i>&nbsp; <%-- <%=userInfo.getName() %> --%>&nbsp; <b class="caret"></b>
						</a>
						<ul class="dropdown-menu">			
							<li>
								<a href="#">Profile</a>
							</li>				
							<li>
								<a href="#">Change Password</a>
							</li>
							<li class="divider"></li>
							<li>
								<s:a action="logout">Logout</s:a>
							</li>
						</ul>
					</li>
				</ul>
				
			</div>
		</div>
	</div>
	
	<div id="pageContent">
		<div class="container">
			<decorator:body />
		</div>
	</div>

	<div id="pageFooter">
		<div class="container">
			<div class="wrap-content">
				&copy; XAPHE Co., Ltd <br/>
				All rights reserved.
			</div>
		</div>
	</div>

</body>

</html>

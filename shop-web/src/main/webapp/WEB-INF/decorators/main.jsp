<!DOCTYPE html>

<%@include file="/jsp/includes/taglib.inc" %>

<%-- <%@ page import="vn.ecreation.jcms.business.common.UserInfo" %>
<%@ page import="vn.ecreation.jcms.core.common.CommonConstant" %> --%>

<html>
<head>
	<title><decorator:title default="Blank Page"/></title>
	
	<s:head/>

    <link href="<s:url value='/styles/site/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/styles/site/font-awesome/css/font-awesome.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    
    <link href="<s:url value='/styles/site/pickadate/classic.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/styles/site/pickadate/classic.date.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/styles/site/pickadate/classic.time.css'/>" rel="stylesheet" type="text/css" media="all"/>
    
    <link href="<s:url value='/styles/site/chosen/chosen.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    
    <!--[if IE 7]>
		<link rel="stylesheet" href="<s:url value='/styles/site/font-awesome/font-awesome-ie7.min.css'/>">
	<![endif]-->
	
	<link href="<s:url value='/styles/site/layout-main.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/styles/site/styles.css'/>" rel="stylesheet" type="text/css" media="all"/>
    
    <!--[if lt IE 9]>
		<script src="<s:url value='/scripts/html5/html5shiv.js'/>"></script>
	<![endif]-->
	
	<decorator:head/>
</head>
<body<decorator:getProperty property="body.id" writeEntireProperty="true" />>
	
<%-- 	<% 
	UserInfo userInfo = UserInfo.getFromSession();
	%> --%>

	<div id="pageHeader">
		<div class="container">
			<div class="wrapper">
				<div class="menu-app pull-left">
					<s:a action="admin/menu" namespace="/">
						<i class="icon-windows"></i>&nbsp; XAPHE - SHOP ONLINE
					</s:a>
				</div>
				
<%-- 				<%if (userInfo != null ){ %> --%>
					<ul class="nav pull-right" id="nav-user">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="nav-user-pulldown">
								<img src="<s:url value="/styles/images/avatar-default.png"/>" class="avatar" />&nbsp; <%-- <%=userInfo.getName() %> --%>&nbsp; <i class="icon-angle-down"></i>
							</a>
							<ul class="dropdown-menu" id="user-dropdown">			
								<li>
									<a href="#"><i class="icon-user"></i>&nbsp; Profile</a>
								</li>				
								<li>
									<a href="#"><i class="icon-key"></i>&nbsp;Change Password</a>
								</li>
								<%-- <%if ( CommonConstant.ROLE_ADMIN.equals(userInfo.getRoleName()) ){ %> --%>
								<li class="divider"></li>
								<li>
									<s:a action="admin" namespace="/"><i class="icon-cog"></i>&nbsp;Admin</s:a>
								</li>
							<%-- 	<%} %> --%>
								<li class="divider"></li>
								<li>
									<s:a action="logout" namespace="/"><i class="icon-off"></i>&nbsp;Logout</s:a>
								</li>
							</ul>
						</li>
					</ul>
	<%-- 			<%} else { %> --%>
					<s:a action="login" namespace="/" cssClass="btn btn-primary login">
						Login
					</s:a>
		<%-- 		<%} %> --%>
				
			</div>
		</div>
	</div>
		
	<div id="pageContent">
	
		<div class="container container1">
			<decorator:body />
		</div>
		
	</div>
	
	<div id="pageFooter">
		<div class="container">
			<div class="wrap-content">
				&copy; XAPHE Co., Ltd <br/>
				All rights reserved.
			</div>
		</div>
	</div>

	<script src="<s:url value='/scripts/jquery/jquery-1.10.2.min.js'/>"></script>
	<script src="<s:url value='/scripts/bootstrap/bootstrap.min.js'/>"></script>
	<script src="<s:url value='/scripts/pickadate/legacy.js'/>"></script>
	<script src="<s:url value='/scripts/pickadate/picker.js'/>"></script>
	<script src="<s:url value='/scripts/pickadate/picker.date.js'/>"></script>
	<script src="<s:url value='/scripts/pickadate/picker.time.js'/>"></script>
	
	<script src="<s:url value='/scripts/chosen/chosen.jquery.min.js'/>"></script>
	
	<script src="<s:url value='/scripts/jcms/core/jcms.common.js'/>"></script>
	<script src="<s:url value='/scripts/jcms/core/jcms.js'/>"></script>
	<!-- 
	<script src="<s:url value='/scripts/jcms/widgets/*.js'/>"></script>
	<script src="<s:url value='/scripts/jcms/plugins/*.js'/>"></script>
	 -->
	<script src="<s:url value='/scripts/jcms/pages/pages.js'/>"></script>
	<script src="<s:url value='/scripts/jcms/pages/page-0309.js'/>"></script>
	
	<script type="text/javascript">
	$(function(){
		JCMS.pages.startUp();
	});
	</script>
	
	<script type="text/javascript">
	<decorator:getProperty property="page.script"/>
	</script>

</body>

</html>

<!DOCTYPE html>

<%@include file="/jsp/includes/taglib.inc" %>
<%-- <%@page import="vn.ecreation.jcms.core.common.Utils"%> --%>

<%-- <%@ page import="vn.ecreation.jcms.business.common.UserInfo" %> --%>
<%-- <%@ page import="vn.ecreation.jcms.core.common.CommonConstant" %> --%>
<html lang="en">
<head>
	<title><decorator:title default="Blank Page"/></title>

	<s:head/>

    <link href="<s:url value='/styles/site/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/styles/site/font-awesome/css/font-awesome.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    
    <link href="<s:url value='/styles/site/pickadate/classic.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/styles/site/pickadate/classic.date.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/styles/site/pickadate/classic.time.css'/>" rel="stylesheet" type="text/css" media="all"/>
    
    <link href="<s:url value='/styles/site/chosen/chosen.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    
    <!--[if IE 7]>
		<link rel="stylesheet" href="<s:url value='/styles/site/font-awesome/font-awesome-ie7.min.css'/>">
	<![endif]-->
	
	<link href="<s:url value='/styles/site/layout-frontend.css'/>" rel="stylesheet" type="text/css" media="all"/>
	
    <!--[if lt IE 9]>
		<script src="<s:url value='/scripts/html5/html5shiv.js'/>"></script>
	<![endif]-->

<decorator:head/>
</head>
<body<decorator:getProperty property="body.id" writeEntireProperty="true" />>
	
<%-- 	<% 
	UserInfo userInfo = UserInfo.getFromSession();
	%> --%>
	
	<s:bean name="vn.xaphe.shop.online.core.modules.admin.item.service.ItemService" var="adminItemService" />
	<s:set var="itemDetailList" value="#adminItemService.itemsDetail"></s:set>
	<s:set var="listListItem" value="#adminItemService.itemsDetail"></s:set>
			
	<header>
		<div id="page-top">
			<div class="container">
				<div class="logo">
					<s:a action="home" namespace="/">
						<i class="icon-windows"></i> XAPHE - SHOP ONLINE
					</s:a>
				</div>
				
				<div class="login-box pull-right">

<%-- 					<% if ( userInfo == null ) {%> --%>

					<s:form action="doLogin" namespace="/" cssClass="form-inline">
						<s:textfield name="username" placeholder="%{getText('label.login.username')}" cssClass="form-control" />
						<s:password name="password" placeholder="%{getText('label.login.password')}" cssClass="form-control" />
						<button type="submit" class="btn btn-success">Sign in</button>
						<s:hidden value="/home" name="returnUrl"></s:hidden>
					</s:form>

<%-- 					<% } else {  %>
					Hello, <%=userInfo.getName() %> &nbsp; (<s:a action="logout" namespace="/">Logout</s:a>)
					<% } %> --%>

				</div>
			</div>
		</div>
	</header>
	
	<section id="page-title">
		<div class="container">
			<span class="title">
				<i class="icon-home"></i>
			</span>
			<ul class="nav navbar-nav pull-right top-nav-2">
				<%-- <% if ( userInfo != null && CommonConstant.ROLE_ADMIN.equals(userInfo.getRoleName()) ) {%> --%>
				<li><s:a action="menu" namespace="/admin">MENU</s:a></li>
				<%--     <%} %> --%>
			</ul>
		</div>
	</section>

	<div id="page-content">

		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<script>
						$(document).ready(
								function() {
									$('label.tree-toggler').click(
											function() {
												$(this).parent().children(
														'ul.tree').toggle(300);
											});
								});
					</script>
					<div class="well" style="width: 300px; padding: 8px 0;">
						<div
							style="overflow-y: scroll; overflow-x: hidden; height: 500px;">
							<ul class="nav nav-list">
								<s:iterator value="itemDetailList" var="itemDetail">
									<li><label class="label.tree-toggler nav-header"><s:property
												value="categoryName" /></label>
										<ul class="nav nav-list tree">
											<s:iterator value="%{#itemDetail.itemList}" var="item">
												<li><s:a action="detail" namespace="/item">
														<s:param name="id" value="%{#item.id}" />
														<s:property value="name" />
													</s:a></li>
											</s:iterator>
										</ul></li>
									<li class="divider"></li>
								</s:iterator>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-9">
					<decorator:body />
				</div>
			</div>
		</div>

	</div>
	
	<div id="page-footer">
		<div class="container">
			<div class="wrap-content">
				&copy; XAPHE Co., Ltd <br/>
				All rights reserved.
			</div>
		</div>
	</div>
	
	<script src="<s:url value='/scripts/jquery/jquery-1.10.2.min.js'/>"></script>
	<script src="<s:url value='/scripts/bootstrap/bootstrap.min.js'/>"></script>
	<script src="<s:url value='/scripts/pickadate/legacy.js'/>"></script>
	<script src="<s:url value='/scripts/pickadate/picker.js'/>"></script>
	<script src="<s:url value='/scripts/pickadate/picker.date.js'/>"></script>
	<script src="<s:url value='/scripts/pickadate/picker.time.js'/>"></script>
	
	<script src="<s:url value='/scripts/chosen/chosen.jquery.min.js'/>"></script>
	
	<script src="<s:url value='/scripts/jcms/core/jcms.common.js'/>"></script>
	<script src="<s:url value='/scripts/jcms/core/jcms.js'/>"></script>
	<!-- 
	<script src="<s:url value='/scripts/jcms/widgets/*.js'/>"></script>
	<script src="<s:url value='/scripts/jcms/plugins/*.js'/>"></script>
	 -->
	<script src="<s:url value='/scripts/jcms/pages/pages.js'/>"></script>
	<script src="<s:url value='/scripts/jcms/pages/page-0309.js'/>"></script>
	
	<script type="text/javascript">
	$(function(){
		JCMS.pages.startUp();
	});
	</script>
	
	<script type="text/javascript">
	<decorator:getProperty property="page.script"/>
	</script>

</body>

</html>

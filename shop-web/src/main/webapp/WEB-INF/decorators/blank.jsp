<!DOCTYPE html>

<%@include file="/jsp/includes/taglib.inc" %>

<html>
<head>	
	<title><decorator:title default="Blank Page"/></title>
	<s:head/>

    <link href="<s:url value='/styles/site/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    <link href="<s:url value='/styles/site/font-awesome/css/font-awesome.min.css'/>" rel="stylesheet" type="text/css" media="all"/>
    
    <!--[if IE 7]>
		<link rel="stylesheet" href="<s:url value='/styles/site/font-awesome/font-awesome-ie7.min.css'/>">
	<![endif]-->
	
    <!--[if lt IE 9]>
		<script src="<s:url value='/scripts/html5/html5shiv.js'/>"></script>
	<![endif]-->
	
	<script src="<s:url value='/scripts/jquery/jquery-1.10.2.min.js'/>"></script>
	<script src="<s:url value='/scripts/bootstrap/bootstrap.min.js'/>"></script>
	
	<link href="<s:url value='/styles/site/layout-blank.css'/>" rel="stylesheet" type="text/css" media="all"/>
	
	<decorator:head/>
</head>
<body<decorator:getProperty property="body.id" writeEntireProperty="true" />>
    <decorator:body/>
</body>
</html>

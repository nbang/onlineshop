@echo off

del autocode\*.* /F /S /Q
rmdir autocode\java\* /S /Q
rmdir autocode\resources\* /S /Q

java -classpath addLimitOffetPlugin.jar;mybatis-generator-core-1.3.2.jar org.mybatis.generator.api.ShellRunner -configfile gencode.xml

REM copy to domain
xcopy autocode ..\..\shop-domain\src\main /S /W /F /Y

del autocode\*.* /F /S /Q
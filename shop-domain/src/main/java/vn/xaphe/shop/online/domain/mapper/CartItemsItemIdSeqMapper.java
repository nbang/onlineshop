package vn.xaphe.shop.online.domain.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import vn.xaphe.shop.online.domain.model.CartItemsItemIdSeq;
import vn.xaphe.shop.online.domain.model.CartItemsItemIdSeqExample;

public interface CartItemsItemIdSeqMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.cart_items_item_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int countByExample(CartItemsItemIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.cart_items_item_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int deleteByExample(CartItemsItemIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.cart_items_item_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int insert(CartItemsItemIdSeq record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.cart_items_item_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int insertSelective(CartItemsItemIdSeq record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.cart_items_item_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    List<CartItemsItemIdSeq> selectByExample(CartItemsItemIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.cart_items_item_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int updateByExampleSelective(@Param("record") CartItemsItemIdSeq record, @Param("example") CartItemsItemIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.cart_items_item_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int updateByExample(@Param("record") CartItemsItemIdSeq record, @Param("example") CartItemsItemIdSeqExample example);
}
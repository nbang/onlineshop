package vn.xaphe.shop.online.domain.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import vn.xaphe.shop.online.domain.model.CartsUserIdSeq;
import vn.xaphe.shop.online.domain.model.CartsUserIdSeqExample;

public interface CartsUserIdSeqMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_user_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int countByExample(CartsUserIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_user_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int deleteByExample(CartsUserIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_user_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int insert(CartsUserIdSeq record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_user_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int insertSelective(CartsUserIdSeq record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_user_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    List<CartsUserIdSeq> selectByExample(CartsUserIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_user_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int updateByExampleSelective(@Param("record") CartsUserIdSeq record, @Param("example") CartsUserIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_user_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int updateByExample(@Param("record") CartsUserIdSeq record, @Param("example") CartsUserIdSeqExample example);
}
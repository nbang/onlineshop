package vn.xaphe.shop.online.domain.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import vn.xaphe.shop.online.domain.model.CartsIdSeq;
import vn.xaphe.shop.online.domain.model.CartsIdSeqExample;

public interface CartsIdSeqMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int countByExample(CartsIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int deleteByExample(CartsIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int insert(CartsIdSeq record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int insertSelective(CartsIdSeq record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    List<CartsIdSeq> selectByExample(CartsIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int updateByExampleSelective(@Param("record") CartsIdSeq record, @Param("example") CartsIdSeqExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table public.carts_id_seq
     *
     * @mbggenerated Mon Aug 12 21:24:56 ICT 2013
     */
    int updateByExample(@Param("record") CartsIdSeq record, @Param("example") CartsIdSeqExample example);
}
/**
 * CategoryService
 */
package vn.xaphe.shop.online.core.modules.admin.category.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.xaphe.shop.online.domain.mapper.CategoriesMapper;
import vn.xaphe.shop.online.domain.model.Categories;
import vn.xaphe.shop.online.domain.model.CategoriesExample;

/**
 * @author donavo
 * @version 0.1
 */
@Service
public class CategoryService {

	@Autowired
	CategoriesMapper categoryMapper;

	public int insert(Categories entity) {
		return categoryMapper.insertSelective(entity);
	}

	public void update(Categories entity) {
		this.categoryMapper.updateByPrimaryKey(entity);
	}

	public void delete(Long id) {
		this.categoryMapper.deleteByPrimaryKey(id);
	}

	public Categories getCategoryById(Long id) {
		return this.categoryMapper.selectByPrimaryKey(id);
	}

	public List<Categories> getListCategory() {
		return categoryMapper.selectByExample(new CategoriesExample());
	}
}

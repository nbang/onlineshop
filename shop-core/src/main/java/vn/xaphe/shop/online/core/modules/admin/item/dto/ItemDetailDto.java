/**
 * 
 */
package vn.xaphe.shop.online.core.modules.admin.item.dto;

import java.util.List;

import vn.xaphe.shop.online.domain.model.Items;

/**
 * Item detail DTO
 * @author son-vo
 * @version 0.1
 */
public class ItemDetailDto {
	private List<Items> itemList;
	private Long categoryId;
	private String categoryName;

	/**
	 * @return the itemList
	 */
	public List<Items> getItemList() {
		return itemList;
	}

	/**
	 * @param itemList
	 *            the itemList to set
	 */
	public void setItemList(List<Items> itemList) {
		this.itemList = itemList;
	}

	/**
	 * @return the categoryId
	 */
	public Long getCategoryId() {
		return categoryId;
	}

	/**
	 * @param categoryId
	 *            the categoryId to set
	 */
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}

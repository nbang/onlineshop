/**
 * 
 */
package vn.xaphe.shop.online.core.modules.item.dto;

import vn.xaphe.shop.online.domain.model.Items;

/**
 * @author nbang
 * @version 0.1
 */
public class ItemInfoDto extends Items {
	private String categoryName;

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}

package vn.xaphe.shop.online.core.modules.item.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.xaphe.shop.online.core.modules.item.dto.ItemInfoDto;
import vn.xaphe.shop.online.core.modules.item.mapper.ItemInfoMapper;

/**
 * @author bang
 *
 */
@Service
public class ItemService {

	@Autowired
	ItemInfoMapper itemsInfoMapper;

	public List<ItemInfoDto> getListItem() {
		List<ItemInfoDto> result = itemsInfoMapper.selectItemInfo();
		
		return result;
	}
	
}
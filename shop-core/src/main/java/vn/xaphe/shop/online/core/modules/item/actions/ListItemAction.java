package vn.xaphe.shop.online.core.modules.item.actions;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.modules.item.service.ItemService;
import vn.xaphe.shop.online.core.modules.item.viewmodel.ListItemViewModel;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/**
 * @author bang
 *
 */
public class ListItemAction extends ActionSupport implements
		ModelDriven<ListItemViewModel> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2240835609113225279L;

	/**
	 * 
	 */

	@Autowired
	private ItemService itemService;
	
	private ListItemViewModel listItemModel = new ListItemViewModel();

	public String execute() {
		this.listItemModel.getItemInfoList().addAll(itemService.getListItem());
		
		return SUCCESS;
	}

	@Override
	public ListItemViewModel getModel() {
		return this.listItemModel;
	}

}

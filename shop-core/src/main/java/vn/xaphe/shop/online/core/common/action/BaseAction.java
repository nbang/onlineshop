package vn.xaphe.shop.online.core.common.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import vn.xaphe.shop.online.core.common.MessageProvider;

import com.opensymphony.xwork2.ActionSupport;

public abstract class BaseAction extends ActionSupport implements
		ServletRequestAware, ServletResponseAware {

	private static final long serialVersionUID = -2221005204423312947L;
	protected static final String HOME = "home";
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected MessageProvider messageProvider;

	@Override
	public String execute() throws Exception {

		if (LOG.isDebugEnabled()) {
			LOG.debug("ACTION BEGIN: " + this.getClass().getName());
		}

		String result;

		try {
			result = this.onExcute();
		} catch (Exception e) {
			LOG.error("ACTION ERROR: " + e.getMessage(), e);
			throw new Exception(e);
		} finally {
			if (LOG.isDebugEnabled()) {
				LOG.debug("ACTION END: " + this.getClass().getName());
			}
		}

		return result;
	}

	protected abstract String onExcute();

	public HttpServletRequest getServletRequest() {
		return request;
	}

	public void setServletRequest(HttpServletRequest req) {
		this.request = req;
	}

	public HttpServletResponse getServletResponse() {
		return response;
	}

	public void setServletResponse(HttpServletResponse resp) {
		this.response = resp;
	}

	public MessageProvider getMessageProvider() {
		if (this.messageProvider == null) {
			this.messageProvider = new MessageProvider(this);
		}
		return this.messageProvider;
	}

	public String getValidateRequiredMessage(String field) {
		return getMessageProvider().getValidateRequired(field);
	}
}

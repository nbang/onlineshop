/**
 * GlobalMessage
 */
package vn.xaphe.shop.online.core.common;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

/**
 * Global Message
 * 
 * @author donavo
 * @version 0.1
 */
public class GlobalMessage {

	private static final String RESOUCE_NAME = "globals";

	protected static Logger LOG = LoggerFactory.getLogger(GlobalMessage.class);

	private static ResourceBundle messages;

	static {
		try {
			messages = ResourceBundle.getBundle(RESOUCE_NAME);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	public static String getMessage(String id) {
		return getMessage(id, true);
	}

	public static String getMessage(String id, Object[] args) {
		return getMessage(id, args, true);
	}

	public static String getMessage(String id, boolean msgOnly) {
		String message = messages.getString(id);

		if (message == null) {
			LOG.error("WARNING: No message for " + id);
		}
		if (!msgOnly) {
			message = "[" + id + "] " + message;
		}
		return message;
	}

	public static String getMessage(String id, Object[] args, boolean msgOnly) {
		String message = messages.getString(id);

		if (message == null) {
			LOG.error("WARNING: No message for " + id);
		}
		if (!msgOnly) {
			message = "[" + id + "] " + message;
		}
		return MessageFormat.format(message, args);
	}

}

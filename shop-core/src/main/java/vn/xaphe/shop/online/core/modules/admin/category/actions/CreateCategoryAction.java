/**
 * 
 */
package vn.xaphe.shop.online.core.modules.admin.category.actions;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.category.service.CategoryService;
import vn.xaphe.shop.online.core.modules.admin.category.viewmodel.CategoryViewModel;
import vn.xaphe.shop.online.core.utils.StringUtils;
import vn.xaphe.shop.online.domain.model.Categories;

import com.opensymphony.xwork2.ModelDriven;

/**
 * Create Category Class
 * 
 * @author donavo
 * @version 0.1
 */
public class CreateCategoryAction extends BaseAction implements
		ModelDriven<CategoryViewModel> {

	private static final long serialVersionUID = 251621933740090310L;
	@Autowired
	private CategoryService categoryService;
	private CategoryViewModel categogyViewModel = new CategoryViewModel();

	@Override
	public CategoryViewModel getModel() {
		return this.categogyViewModel;
	}

	/**
	 * @return
	 */
	public String onExcute() {
		Categories entity = new Categories();
		entity.setName(categogyViewModel.getCategoryName());
		if (categogyViewModel.getId() == null) {
			categoryService.insert(entity);
		} else {
			entity.setId(categogyViewModel.getId());
			categoryService.update(entity);
		}
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (!StringUtils.hasLength(categogyViewModel.getCategoryName())) {
			addFieldError("categoryName",
					getValidateRequiredMessage("label.category.name"));
		}
	}
}

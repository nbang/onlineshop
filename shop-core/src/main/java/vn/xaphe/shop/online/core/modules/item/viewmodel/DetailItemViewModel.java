package vn.xaphe.shop.online.core.modules.item.viewmodel;

import vn.xaphe.shop.online.core.modules.item.dto.ItemDetailDto;

/**
 * @author bang
 * 
 */
public class DetailItemViewModel {

	private ItemDetailDto items = null;

	public ItemDetailDto getItemInfoList() {
		return this.items;
	}

	public void setItemInfoList(ItemDetailDto items) {
		this.items = items;
	}
}

package vn.xaphe.shop.online.core.modules.admin.item.viewmodel;

import java.util.ArrayList;
import java.util.List;

import vn.xaphe.shop.online.core.modules.admin.item.dto.ItemDetailDto;

public class ListAdminItemViewModel {

	private List<ItemDetailDto> itemDetailDTOList = new ArrayList<ItemDetailDto>();

	/**
	 * @return the itemDetailDTOList
	 */
	public List<ItemDetailDto> getItemDetailDTOList() {
		return itemDetailDTOList;
	}

	/**
	 * @param itemDetailDTOList
	 *            the itemDetailDTOList to set
	 */
	public void setItemDetailDTOList(List<ItemDetailDto> itemDetailDTOList) {
		this.itemDetailDTOList = itemDetailDTOList;
	}

}

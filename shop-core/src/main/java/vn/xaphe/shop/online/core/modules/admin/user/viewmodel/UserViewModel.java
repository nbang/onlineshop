/**
 * 
 */
package vn.xaphe.shop.online.core.modules.admin.user.viewmodel;

/**
 * Category view model
 * 
 * @author quangphan
 * @version 0.1
 */
public class UserViewModel {
	private Long id;
	private String userName;
	private String password;
	private String email;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}



}

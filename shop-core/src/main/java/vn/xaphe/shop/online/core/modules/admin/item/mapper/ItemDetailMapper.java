/**
 * 
 */
package vn.xaphe.shop.online.core.modules.admin.item.mapper;

import java.util.List;

import vn.xaphe.shop.online.core.modules.admin.item.dto.ItemDetailDto;

/**
 * @author son-vo
 *
 */
public interface ItemDetailMapper {
	List<ItemDetailDto> selectItemsDetail();
	ItemDetailDto selectItemDetail(Long id);
}

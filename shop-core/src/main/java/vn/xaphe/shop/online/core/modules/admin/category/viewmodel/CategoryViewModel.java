/**
 * 
 */
package vn.xaphe.shop.online.core.modules.admin.category.viewmodel;

/**
 * Category view model
 * 
 * @author donavo
 * @version 0.1
 */
public class CategoryViewModel {
	private Long id;
	private String categoryName;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName
	 *            the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}

/**
 * 
 */
package vn.xaphe.shop.online.core.modules.admin.item.actions;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.category.service.CategoryService;
import vn.xaphe.shop.online.core.modules.admin.item.service.ItemService;
import vn.xaphe.shop.online.core.modules.admin.item.viewmodel.AdminItemViewModel;
import vn.xaphe.shop.online.core.utils.StringUtils;
import vn.xaphe.shop.online.domain.model.Items;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

/**
 * Create Items Class
 * 
 * @author donavo
 * @version 0.1
 */
public class CreateItemAction extends BaseAction implements Preparable,
		ModelDriven<AdminItemViewModel> {

	private static final long serialVersionUID = -914548980091378068L;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ItemService itemService;
	private AdminItemViewModel itemViewModel = new AdminItemViewModel();

	@Override
	public void prepare() throws Exception {
		itemViewModel.setCategoryList(categoryService.getListCategory());
	}

	@Override
	public AdminItemViewModel getModel() {
		return this.itemViewModel;
	}

	@Override
	public String onExcute() {
		Items entity = new Items();
		entity.setId(itemViewModel.getId());
		entity.setName(itemViewModel.getItemName());
		entity.setPrice(itemViewModel.getPrice());
		entity.setDescription(itemViewModel.getDescription());
		entity.setCategoryId(itemViewModel.getCategoryId());
		entity.setDescription(itemViewModel.getDescription());
		itemService.insertOrUpdate(entity);
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (itemViewModel.getCategoryId() == null) {
			addFieldError("categoryName",
					getValidateRequiredMessage("label.category.name"));
		}
		if (!StringUtils.hasLength(itemViewModel.getItemName())) {
			addFieldError("itemName",
					getValidateRequiredMessage("label.item.name"));
		}
	}
}

package vn.xaphe.shop.online.core.modules.item.viewmodel;

import java.util.ArrayList;
import java.util.List;

import vn.xaphe.shop.online.core.modules.item.dto.ItemInfoDto;

/**
 * @author bang
 * 
 */
public class ListItemViewModel {

	private List<ItemInfoDto> items = new ArrayList<ItemInfoDto>();

	public List<ItemInfoDto> getItemInfoList() {
		return this.items;
	}
}

/**
 * 
 */
package vn.xaphe.shop.online.core.modules.item.mapper;

import java.util.List;

import vn.xaphe.shop.online.core.modules.item.dto.ItemInfoDto;

/**
 * @author nbang
 * 
 */
public interface ItemInfoMapper {
	List<ItemInfoDto> selectItemInfo();
}
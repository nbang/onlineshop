/**
 * BaseService
 */
package vn.xaphe.shop.online.core.common.service;

import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

public class BaseService {

	protected static Logger LOG = LoggerFactory.getLogger(BaseService.class);

	public BaseService() {

	}

//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	protected void setHeaderInfo(Object obj, Boolean isNew) {
//		if (obj != null) {
//			Class objClass = obj.getClass();
//
//			UserInfo userInfo = UserInfo.getFromSession();
//			if (userInfo != null) {
//
//				if (isNew) {
//
//					// Update createdBy
//					Method methodCreatedBy = null;
//
//					try {
//						methodCreatedBy = objClass.getMethod("setCreatedBy",
//								Long.class);
//					} catch (NoSuchMethodException e) {
//						LOG.error(e.getMessage(), e);
//					} catch (SecurityException e) {
//						LOG.error(e.getMessage(), e);
//					}
//
//					if (methodCreatedBy != null) {
//						try {
//							methodCreatedBy.invoke(obj, userInfo.getUserId());
//						} catch (IllegalAccessException e) {
//							LOG.error(e.getMessage(), e);
//						} catch (IllegalArgumentException e) {
//							LOG.error(e.getMessage(), e);
//						} catch (InvocationTargetException e) {
//							LOG.error(e.getMessage(), e);
//						}
//					}
//
//					// Update createdTime
//					Method methodCreatedTime = null;
//
//					try {
//						methodCreatedTime = objClass.getMethod(
//								"setCreatedTime", Date.class);
//					} catch (NoSuchMethodException e) {
//						LOG.error(e.getMessage(), e);
//					} catch (SecurityException e) {
//						LOG.error(e.getMessage(), e);
//					}
//
//					if (methodCreatedTime != null) {
//						try {
//							methodCreatedTime.invoke(obj, Calendar
//									.getInstance().getTime());
//						} catch (IllegalAccessException e) {
//							LOG.error(e.getMessage(), e);
//						} catch (IllegalArgumentException e) {
//							LOG.error(e.getMessage(), e);
//						} catch (InvocationTargetException e) {
//							LOG.error(e.getMessage(), e);
//						}
//					}
//				}
//
//				// Update updatedBy
//				Method methodUpdatedBy = null;
//				// createdBy
//				try {
//					methodUpdatedBy = objClass.getMethod("setUpdatedBy",
//							Long.class);
//				} catch (NoSuchMethodException e) {
//					LOG.error(e.getMessage(), e);
//				} catch (SecurityException e) {
//					LOG.error(e.getMessage(), e);
//				}
//
//				if (methodUpdatedBy != null) {
//					try {
//						methodUpdatedBy.invoke(obj, userInfo.getUserId());
//					} catch (IllegalAccessException e) {
//						LOG.error(e.getMessage(), e);
//					} catch (IllegalArgumentException e) {
//						LOG.error(e.getMessage(), e);
//					} catch (InvocationTargetException e) {
//						LOG.error(e.getMessage(), e);
//					}
//				}
//
//				// updatedTime
//				Method methodUpdatedTime = null;
//
//				try {
//					methodUpdatedTime = objClass.getMethod("setUpdatedTime",
//							Date.class);
//				} catch (NoSuchMethodException e) {
//					LOG.error(e.getMessage(), e);
//				} catch (SecurityException e) {
//					LOG.error(e.getMessage(), e);
//				}
//
//				if (methodUpdatedTime != null) {
//					try {
//						methodUpdatedTime.invoke(obj, Calendar.getInstance()
//								.getTime());
//					} catch (IllegalAccessException e) {
//						LOG.error(e.getMessage(), e);
//					} catch (IllegalArgumentException e) {
//						LOG.error(e.getMessage(), e);
//					} catch (InvocationTargetException e) {
//						LOG.error(e.getMessage(), e);
//					}
//				}
//			}
//		}
//	}
}

package vn.xaphe.shop.online.core.modules.admin.user.actions;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.user.service.UserService;

/**
 * Delete user class
 * @author quangphan
 * @version 0.1
 */
public class DeleteUserAction extends BaseAction {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4997923348279533353L;
	@Autowired
	private UserService userService;
	private Long id;

	@Override
	protected String onExcute() {
		this.userService.delete(this.id);
		return SUCCESS;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

}

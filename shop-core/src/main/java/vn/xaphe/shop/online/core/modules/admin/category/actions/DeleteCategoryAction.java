package vn.xaphe.shop.online.core.modules.admin.category.actions;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.category.service.CategoryService;

/**
 * Delete category class
 * @author donavo
 * @version 0.1
 */
public class DeleteCategoryAction extends BaseAction {
	private static final long serialVersionUID = 5640852262361902501L;
	@Autowired
	private CategoryService categoryService;
	private Long id;

	@Override
	protected String onExcute() {
		this.categoryService.delete(this.id);
		return SUCCESS;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

}

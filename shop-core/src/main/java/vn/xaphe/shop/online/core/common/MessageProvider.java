/**
 * MessageProvider
 */
package vn.xaphe.shop.online.core.common;

import java.util.List;
import java.util.ResourceBundle;

import com.opensymphony.xwork2.TextProvider;
import com.opensymphony.xwork2.util.ValueStack;

/**
 * Message provider
 * 
 * @author donavo
 * @version 0.1
 */
public class MessageProvider implements TextProvider {
	private static final String VALIDATION_REQUIRED = "validation.required";
	private TextProvider textProvider;

	public MessageProvider() {

	}

	public MessageProvider(TextProvider textProvider) {
		this.textProvider = textProvider;
	}

	/**
	 * @return the textProvider
	 */
	public TextProvider getTextProvider() {
		return textProvider;
	}

	/**
	 * @param textProvider
	 *            the textProvider to set
	 */
	public void setTextProvider(TextProvider textProvider) {
		this.textProvider = textProvider;
	}

	public boolean hasKey(String key) {
		return getTextProvider().hasKey(key);
	}

	public String getText(String aTextName) {
		return getTextProvider().getText(aTextName);
	}

	public String getText(String aTextName, String defaultValue) {
		return getTextProvider().getText(aTextName, defaultValue);
	}

	public String getText(String aTextName, String defaultValue, String obj) {
		return getTextProvider().getText(aTextName, defaultValue, obj);
	}

	public String getText(String aTextName, List<?> args) {
		return getTextProvider().getText(aTextName, args);
	}

	public String getText(String key, String[] args) {
		return getTextProvider().getText(key, args);
	}

	public String getText(String aTextName, String defaultValue, List<?> args) {
		return getTextProvider().getText(aTextName, defaultValue, args);
	}

	public String getText(String key, String defaultValue, String[] args) {
		return getTextProvider().getText(key, defaultValue, args);
	}

	public String getText(String key, String defaultValue, List<?> args,
			ValueStack stack) {
		return getTextProvider().getText(key, defaultValue, args, stack);
	}

	public String getText(String key, String defaultValue, String[] args,
			ValueStack stack) {
		return getTextProvider().getText(key, defaultValue, args, stack);
	}

	public ResourceBundle getTexts() {
		return getTextProvider().getTexts();
	}

	public ResourceBundle getTexts(String aBundleName) {
		return getTextProvider().getTexts(aBundleName);
	}

	/**
	 * Validation Message - BEGIN
	 */
	public String getValidateRequired(String field) {
		return GlobalMessage.getMessage(MessageProvider.VALIDATION_REQUIRED,
				new Object[] { this.getText(field) });
		/*
		 * return this.getText(GlobalMessage.VALIDATION_REQUIRED, new String[] {
		 * this.getText(field) });
		 */
	}
	/**
	 * Validation Message - END
	 */
}

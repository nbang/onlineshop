/**
 * 
 */
package vn.xaphe.shop.online.core.utils;

/**
 * @author donavo
 * 
 */
public class StringUtils {
	
	/**
	 * Check string is blank or not
	 * @param str
	 * @return boolean
	 */
	public static boolean hasLength(String str) {
		if (str == null || str.length() == 0) {
			return false;
		}
		return true;
	}
}

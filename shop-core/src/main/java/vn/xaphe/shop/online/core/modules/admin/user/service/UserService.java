/**
 * UserService
 */
package vn.xaphe.shop.online.core.modules.admin.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import vn.xaphe.shop.online.domain.mapper.UsersMapper;
import vn.xaphe.shop.online.domain.model.Users;
import vn.xaphe.shop.online.domain.model.UsersExample;

/**
 * @author quangphan
 * @version 0.1
 */
@Service
public class UserService {

	@Autowired
	UsersMapper userMapper;

	public int insert(Users entity) {
		return userMapper.insertSelective(entity);
	}

	public void update(Users entity) {
		this.userMapper.updateByPrimaryKey(entity);
	}

	public void delete(Long id) {
		this.userMapper.deleteByPrimaryKey(id);
	}

	public Users getUserById(Long id) {
		return this.userMapper.selectByPrimaryKey(id);
	}

	public List<Users> getListUser() {
		return userMapper.selectByExample(new UsersExample());
	}
}

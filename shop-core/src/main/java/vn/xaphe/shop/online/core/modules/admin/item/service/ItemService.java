/**
 * 
 */
package vn.xaphe.shop.online.core.modules.admin.item.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.xaphe.shop.online.core.modules.admin.item.dto.ItemDetailDto;
import vn.xaphe.shop.online.core.modules.admin.item.mapper.ItemDetailMapper;
import vn.xaphe.shop.online.domain.mapper.ItemsMapper;
import vn.xaphe.shop.online.domain.model.Items;

/**
 * @author son-vo
 *
 */
@Service(value = "adminItemService")
public class ItemService {
	@Autowired
	ItemDetailMapper itemDetailMapper;
	@Autowired
	ItemsMapper itemMapper;

	/**
	 * Get item list group by category
	 * @return List<ItemDetailDto>
	 */
	public List<ItemDetailDto> getItemsDetail() {
		return itemDetailMapper.selectItemsDetail();
	}
	
	/**
	 * Get item detail
	 * @param id
	 * @return ItemDetailDto
	 */
	public ItemDetailDto getItemDetail(Long id) {
		return itemDetailMapper.selectItemDetail(id);
	}

	/**
	 * Insert or update item
	 * @param entity
	 */
	public void insertOrUpdate(Items entity) {
		if (entity.getId() == null) {
			itemMapper.insertSelective(entity);
		} else {
			itemMapper.updateByPrimaryKey(entity);
		}
	}

	/**
	 * Delete item
	 * @param id
	 */
	public void delete(Long id) {
		itemMapper.deleteByPrimaryKey(id);
	}
	
	/**
	 * Get item by primary key
	 * @param id
	 * @return Items
	 */
	public Items getItemById(Long id) {
		return itemMapper.selectByPrimaryKey(id);
	}
}

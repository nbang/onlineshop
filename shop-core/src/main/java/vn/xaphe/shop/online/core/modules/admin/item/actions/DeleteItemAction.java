package vn.xaphe.shop.online.core.modules.admin.item.actions;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.item.service.ItemService;

/**
 * Delete item class
 * @author donavo
 * @version 0.1
 */
public class DeleteItemAction extends BaseAction {
	private static final long serialVersionUID = 5640852262361902501L;
	@Autowired
	private ItemService itemService;
	private Long id;

	@Override
	protected String onExcute() {
		this.itemService.delete(this.id);
		return SUCCESS;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

}

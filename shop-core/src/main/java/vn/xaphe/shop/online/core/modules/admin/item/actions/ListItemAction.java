package vn.xaphe.shop.online.core.modules.admin.item.actions;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.modules.admin.item.dto.ItemDetailDto;
import vn.xaphe.shop.online.core.modules.admin.item.service.ItemService;
import vn.xaphe.shop.online.core.modules.admin.item.viewmodel.ListAdminItemViewModel;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/**
 * List Items class
 * @author donavo
 * @version 0.1
 */
public class ListItemAction extends ActionSupport implements
		ModelDriven<ListAdminItemViewModel> {
	private static final long serialVersionUID = 5181358155351968657L;

	@Autowired
	private ItemService itemService;

	private ListAdminItemViewModel listItemViewModel = new ListAdminItemViewModel();

	public ListAdminItemViewModel getModel() {
		return listItemViewModel;
	}

	public String execute() {
		List<ItemDetailDto> list = itemService.getItemsDetail();
		this.listItemViewModel.getItemDetailDTOList().addAll(list);
		return SUCCESS;
	}

}

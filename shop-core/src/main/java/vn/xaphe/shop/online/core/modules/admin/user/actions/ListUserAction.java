package vn.xaphe.shop.online.core.modules.admin.user.actions;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.modules.admin.user.service.UserService;
import vn.xaphe.shop.online.core.modules.admin.user.viewmodel.ListUserViewModel;
import vn.xaphe.shop.online.domain.model.Users;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ListUserAction extends ActionSupport implements
		ModelDriven<ListUserViewModel> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9108562756965100335L;
	/**
	 * 
	 */


	@Autowired
	private UserService userService;

	private ListUserViewModel listUser = new ListUserViewModel();

	public ListUserViewModel getModel() {
		return listUser;
	}

	public String execute() {

		List<Users> list = userService.getListUser();
		this.listUser.getUserList().addAll(list);

		
		return SUCCESS;
	}

}

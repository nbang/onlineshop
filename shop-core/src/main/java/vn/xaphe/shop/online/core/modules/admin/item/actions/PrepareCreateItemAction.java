/**
 * 
 */
package vn.xaphe.shop.online.core.modules.admin.item.actions;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.category.service.CategoryService;
import vn.xaphe.shop.online.core.modules.admin.item.viewmodel.AdminItemViewModel;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

/**
 * Prepare To Create Item Class
 * 
 * @author donavo
 * @version 0.1
 */
public class PrepareCreateItemAction extends BaseAction implements Preparable,
		ModelDriven<AdminItemViewModel> {

	private static final long serialVersionUID = 251621933740090310L;
	@Autowired
	private CategoryService categoryService;
	private AdminItemViewModel itemViewModel = new AdminItemViewModel();

	@Override
	public void prepare() throws Exception {
		itemViewModel.setCategoryList(categoryService.getListCategory());
	}

	@Override
	public AdminItemViewModel getModel() {
		return this.itemViewModel;
	}

	@Override
	public String onExcute() {
		return SUCCESS;
	}
}

package vn.xaphe.shop.online.core.modules.admin.category.viewmodel;

import java.util.ArrayList;
import java.util.List;

import vn.xaphe.shop.online.domain.model.Categories;

public class ListCategoryViewModel {

	private List<Categories> categoryList = new ArrayList<Categories>();

	/**
	 * @return the categoryList
	 */
	public List<Categories> getCategoryList() {
		return categoryList;
	}

	/**
	 * @param categoryList
	 *            the categoryList to set
	 */
	public void setCategoryList(List<Categories> categoryList) {
		this.categoryList = categoryList;
	}

}

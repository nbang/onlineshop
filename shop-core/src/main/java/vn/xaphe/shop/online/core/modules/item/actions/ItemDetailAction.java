/**
 * 
 */
package vn.xaphe.shop.online.core.modules.item.actions;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.item.dto.ItemDetailDto;
import vn.xaphe.shop.online.core.modules.admin.item.service.ItemService;
import vn.xaphe.shop.online.core.modules.admin.item.viewmodel.AdminItemViewModel;

import com.opensymphony.xwork2.ModelDriven;

/**
 * Item detail Class
 * 
 * @author donavo
 * @version 0.1
 */
public class ItemDetailAction extends BaseAction implements
		ModelDriven<AdminItemViewModel> {

	private static final long serialVersionUID = -914548980091378068L;
	@Autowired
	private ItemService itemService;
	private AdminItemViewModel itemViewModel = new AdminItemViewModel();

	@Override
	public AdminItemViewModel getModel() {
		return this.itemViewModel;
	}

	@Override
	public String onExcute() {
		String id = getServletRequest().getParameter("id");
		if (id != null) {
			ItemDetailDto dto = itemService.getItemDetail(Long.parseLong(id));
			itemViewModel.setCategoryId(dto.getCategoryId());
			itemViewModel.setId(dto.getItemList().get(0).getId());
			itemViewModel.setItemName(dto.getItemList().get(0).getName());
			itemViewModel.setPrice(dto.getItemList().get(0).getPrice());
			itemViewModel.setDescription(dto.getItemList().get(0).getDescription());
		}
		return SUCCESS;
	}
}

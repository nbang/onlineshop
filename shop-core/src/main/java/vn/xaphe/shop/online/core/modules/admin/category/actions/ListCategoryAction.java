package vn.xaphe.shop.online.core.modules.admin.category.actions;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.modules.admin.category.service.CategoryService;
import vn.xaphe.shop.online.core.modules.admin.category.viewmodel.ListCategoryViewModel;
import vn.xaphe.shop.online.core.modules.admin.item.service.ItemService;
import vn.xaphe.shop.online.domain.model.Categories;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ListCategoryAction extends ActionSupport implements
		ModelDriven<ListCategoryViewModel> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5181358155351968657L;

	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ItemService itemService;

	private ListCategoryViewModel listCategoryViewModel = new ListCategoryViewModel();

	public ListCategoryViewModel getModel() {
		return listCategoryViewModel;
	}

	public String execute() {

		List<Categories> list = categoryService.getListCategory();
		this.listCategoryViewModel.getCategoryList().addAll(list);

		/*List<ItemDetailDto> dtoList = itemService.getItemsDetail();
		if (dtoList != null) {
			for (ItemDetailDto dto : dtoList) {
				System.out.println(dto.getCategoryName());
				for (Items item : dto.getItemList()) {
					System.out.println(item.getName());
				}
			}
		}*/
		return SUCCESS;
	}

}

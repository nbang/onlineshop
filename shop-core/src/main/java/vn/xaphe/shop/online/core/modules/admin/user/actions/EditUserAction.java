package vn.xaphe.shop.online.core.modules.admin.user.actions;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ModelDriven;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.user.service.UserService;
import vn.xaphe.shop.online.core.modules.admin.user.viewmodel.UserViewModel;
import vn.xaphe.shop.online.domain.model.Users;

/**
 * Edit user class
 * 
 * @author quangphan
 * @version 0.1
 */
public class EditUserAction extends BaseAction implements
		ModelDriven<UserViewModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2986253069650288353L;
	@Autowired
	private UserService userService;
	private UserViewModel userViewModel = new UserViewModel();

	@Override
	public UserViewModel getModel() {
		return this.userViewModel;
	}

	@Override
	protected String onExcute() {
		String id = getServletRequest().getParameter("id");
		if (id != null) {
			Users entity = this.userService.getUserById(Long
					.parseLong(id));
			userViewModel.setUserName(entity.getUsername());
			userViewModel.setEmail(entity.getEmail());
		}
		return SUCCESS;
	}
}
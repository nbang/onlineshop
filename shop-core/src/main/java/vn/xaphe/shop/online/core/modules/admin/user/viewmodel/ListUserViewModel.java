package vn.xaphe.shop.online.core.modules.admin.user.viewmodel;

import java.util.ArrayList;
import java.util.List;

import vn.xaphe.shop.online.domain.model.Users;

public class ListUserViewModel {

	private List<Users> userList = new ArrayList<Users>();


	/**
	 * @return the userList
	 */
	public List<Users> getUserList() {
		return userList;
	}

	/**
	 * @param userList the userList to set
	 */
	public void setUserList(List<Users> userList) {
		this.userList = userList;
	}
	
	
}

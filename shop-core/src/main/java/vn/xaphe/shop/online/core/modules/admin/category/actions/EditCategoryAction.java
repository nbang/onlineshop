package vn.xaphe.shop.online.core.modules.admin.category.actions;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ModelDriven;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.category.service.CategoryService;
import vn.xaphe.shop.online.core.modules.admin.category.viewmodel.CategoryViewModel;
import vn.xaphe.shop.online.domain.model.Categories;

/**
 * Edit category class
 * 
 * @author donavo
 * @version 0.1
 */
public class EditCategoryAction extends BaseAction implements
		ModelDriven<CategoryViewModel> {
	private static final long serialVersionUID = 4189712829993060262L;
	@Autowired
	private CategoryService categoryService;
	private CategoryViewModel categogyViewModel = new CategoryViewModel();

	@Override
	public CategoryViewModel getModel() {
		return this.categogyViewModel;
	}

	@Override
	protected String onExcute() {
		String id = getServletRequest().getParameter("id");
		if (id != null) {
			Categories entity = this.categoryService.getCategoryById(Long
					.parseLong(id));
			categogyViewModel.setCategoryName(entity.getName());
		}
		return SUCCESS;
	}
}
/**
 * 
 */
package vn.xaphe.shop.online.core.modules.admin.user.actions;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.user.service.UserService;
import vn.xaphe.shop.online.core.modules.admin.user.viewmodel.UserViewModel;
import vn.xaphe.shop.online.core.utils.StringUtils;
import vn.xaphe.shop.online.domain.model.Users;

import com.opensymphony.xwork2.ModelDriven;

/**
 * Create User Class
 * 
 * @author quangphan
 * @version 0.1
 */
public class CreateUserAction extends BaseAction implements
		ModelDriven<UserViewModel> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 143095353735953853L;
	@Autowired
	private UserService userService;
	private UserViewModel userViewModel = new UserViewModel();

	@Override
	public UserViewModel getModel() {
		return this.userViewModel;
	}

	/**
	 * @return
	 */
	public String onExcute() {
		Users entity = new Users();
		entity.setUsername(userViewModel.getUserName());
		entity.setPassword(userViewModel.getPassword());
		entity.setEmail(userViewModel.getEmail());
		if (userViewModel.getId() == null) {
			userService.insert(entity);
		} else {
			entity.setId(userViewModel.getId());
			userService.update(entity);
		
		}
		return SUCCESS;
	}

	@Override
	public void validate() {
		if (!StringUtils.hasLength(userViewModel.getUserName())) {
			addFieldError("usernameError",
					getValidateRequiredMessage("label.user.name"));
		}
	}
}

package vn.xaphe.shop.online.core.modules.admin.item.actions;

import org.springframework.beans.factory.annotation.Autowired;

import vn.xaphe.shop.online.core.common.action.BaseAction;
import vn.xaphe.shop.online.core.modules.admin.category.service.CategoryService;
import vn.xaphe.shop.online.core.modules.admin.item.service.ItemService;
import vn.xaphe.shop.online.core.modules.admin.item.viewmodel.AdminItemViewModel;
import vn.xaphe.shop.online.domain.model.Items;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

/**
 * Edit category class
 * 
 * @author donavo
 * @version 0.1
 */
public class EditItemAction extends BaseAction implements Preparable,
		ModelDriven<AdminItemViewModel> {
	private static final long serialVersionUID = 4189712829993060262L;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ItemService itemService;
	private AdminItemViewModel itemViewModel = new AdminItemViewModel();

	@Override
	public void prepare() throws Exception {
		itemViewModel.setCategoryList(categoryService.getListCategory());
	}

	@Override
	public AdminItemViewModel getModel() {
		return this.itemViewModel;
	}

	@Override
	protected String onExcute() {
		String id = getServletRequest().getParameter("id");
		if (id != null) {
			Items entity = this.itemService.getItemById(Long.parseLong(id));
			itemViewModel.setCategoryId(entity.getCategoryId());
			itemViewModel.setItemName(entity.getName());
			itemViewModel.setPrice(entity.getPrice());
			itemViewModel.setDescription(entity.getDescription());
		}
		return SUCCESS;
	}
}